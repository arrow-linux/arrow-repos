# Arrow-Repos
# Open the pacman configuration file.
$ (Your-Text-Editor) /etc/pacman.conf

# add this to the bottom, then save.
$ [Arrow-Repos]
$ SigLevel = Optional TrustAll
$ Server = https://gitlab.com/arrowlinux/arrow-repos.git/$arch

# Update pacman
$ sudo pacman -Syy

# check the list of apps in the repo
$ sudo pacman -Sl --color always Arrow-Repos